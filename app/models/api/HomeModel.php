<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class HomeModel extends CI_Model

{
    public function list_query()
    {

    }

    public function product_get($limit, $offset)
    {
        $path = base_url('assets/uploads/');
        $sql_limit = "";
        if ($limit > 0 && $offset != "") {
            $page = ($offset - 1) * $limit;
            $sql_limit = "LIMIT $page,$limit";
        }
        $query = $this->db->query("SELECT
                                        p.id,
                                        p.code,
                                        CONCAT('" . $path . "',p.image) as image,
                                        p.name,
                                        p.brand,
                                        ROUND(p.price , 2) AS price_usa,
                                        ROUND(p.price  * (SELECT rate FROM sma_currencies WHERE `code`='REL'), 0) AS price_rei,
                                        ROUND(p.price  * (SELECT rate FROM sma_currencies WHERE `code`='ERU'), 2) AS price_eur,
                                        p.product_details,
                                        p.category_id,
                                        IF(f.id,'true','false') as favorite
                                    FROM
                                        sma_products p
                                        LEFT JOIN sma_favorite f
                                      ON f.product_id = p.id AND f.customer_id = 'hf'
                                    ORDER BY p.id DESC
                                    {$sql_limit}
                                    ");

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function product_detail($id)
    {
        $path = base_url('assets/uploads/');

        $where = "";
        if ($id) {
            $where = "p.id = $id";
        }
        $query = $this->db->query("SELECT
                                        p.id,
                                        p.code,
                                        CONCAT('" . $path . "',p.image) as image,
                                        p.name,
                                        ROUND(p.price , 2) AS price_usa,
                                        ROUND(p.price  * (SELECT rate FROM sma_currencies WHERE `code`='REL'), 0) AS price_rei,
                                        ROUND(p.price  * (SELECT rate FROM sma_currencies WHERE `code`='ERU'), 2) AS price_eur,
                                        p.product_details,
                                        p.category_id,
                                        IF(f.id,'true','false') as favorite
                                    FROM
                                        sma_products p
                                        LEFT JOIN sma_favorite f
                                      ON f.product_id = p.id AND f.customer_id = 'null'
                                        WHERE {$where}
                                    ORDER BY p.id DESC");

        return $query->row();
    }

    public function product_detail_customer($id, $customer_id)
    {
        $path = base_url('assets/uploads/');

        $where = "";
        if ($id) {
            $where = "p.id = $id";
        }
        $query = $this->db->query("SELECT
                                        p.id,
                                        p.code,
                                        CONCAT('" . $path . "',p.image) as image,
                                        p.name,
                                        ROUND(p.price , 2) AS price_usa,
                                        ROUND(p.price  * (SELECT rate FROM sma_currencies WHERE `code`='REL'), 0) AS price_rei,
                                        ROUND(p.price  * (SELECT rate FROM sma_currencies WHERE `code`='ERU'), 2) AS price_eur,
                                        p.product_details,
                                        p.category_id,
                                        $customer_id as customer_id,
                                        IF(f.id,'true','false') as favorite
                                    FROM
                                        sma_products p
                                        LEFT JOIN sma_favorite f
                                      ON f.product_id = p.id AND f.customer_id = $customer_id
                                        WHERE {$where}
                                    ORDER BY p.id DESC");

        return $query->row();
    }

    public function product_get_customer($customer_id, $limit, $offset)
    {

        $sql_limit = "";
        if ($limit > 0 && $offset != "") {
            $page = ($offset - 1) * $limit;
            $sql_limit = "LIMIT $page,$limit";
        }
        $path = base_url('assets/uploads/');
        $query = $this->db->query("SELECT
                                        p.id,
                                        p.code,
                                        CONCAT('$path',p.image) as image,
                                        p.name,
                                        p.brand,
                                        ROUND(p.price , 2) AS price_usa,
                                        ROUND(p.price  * (SELECT rate FROM sma_currencies WHERE `code`='REL'), 0) AS price_rei,
                                        ROUND(p.price  * (SELECT rate FROM sma_currencies WHERE `code`='ERU'), 2) AS price_eur,
                                        p.product_details,
                                        p.category_id,
                                        IF(f.id,'true','false') as favorite
                                    FROM 
                                          sma_products p
                                    LEFT JOIN sma_favorite f
                                      ON f.product_id=p.id  AND f.customer_id = $customer_id ORDER BY p.id DESC {$sql_limit}");
        return $query->result();
    }


    public function get_banner()
    {
        $path = base_url('assets/uploads/banner/');

        $query = $this->db->query(" select id,banner_name,CONCAT('" . $path . "',banner_image) as banner_image,status from sma_banner where status='1' ");
        return $query->result();
    }


    public function get_contact_us()
    {
        $query = $this->db->query("SELECT title,phone FROM sma_contact_us");
        return $query->row();
    }

    public function get_about_us()
    {
        $query = $this->db->query("SELECT
                                    title,
                                    description_kh,
                                    description_en
                                    FROM
                                    sma_about_us
                                  ");
        return $query->row();
    }

    public function delivery_price() {
        $query = $this->db->query("SELECT
                                    title,
                                    description_kh
                                    FROM
                                    price_delivery
                                    ")->row();
        return $query;
    }

    public function main_category()
    {
        $query = $this->db->query("
                                    SELECT
                                    c.id,
                                    c.name,
                                    c.parent_id
                                    FROM
                                    sma_categories c
                                    WHERE
                                    (c.parent_id is null or c.parent_id='' or c.parent_id=0)
                                    ORDER BY id DESC
    ");
        return $query->result();
    }

    public function sub_category($category_id)
    {
        $sub = $this->db->query("SELECT id,name,parent_id FROM sma_categories WHERE parent_id = '$category_id' ORDER BY id DESC")->result();
        $data[0] = (object)[
            'id' => 0,
            'code' => '0',
            'name' => 'All',
            'image' => '',
            'slug' => ''
        ];
        foreach ($sub as $b) {
            array_push($data, $b);
        }
        return $data;
    }

    public function pro_sub_category($sub_categoty_id)
    {
        $path = base_url('assets/uploads/');
        $where = "";
        if ($sub_categoty_id) {
            $where .= "category_id = $sub_categoty_id";
        }

        $query = $this->db->query("SELECT
                                        p.id,
                                        p.code,
                                        CONCAT('" . $path . "',p.image) as image,
                                        p.name,
                                       ROUND(p.price , 2) AS price_usa,
                                        ROUND(p.price  * (SELECT rate FROM sma_currencies WHERE `code`='REL'), 0) AS price_rei,
                                        ROUND(p.price  * (SELECT rate FROM sma_currencies WHERE `code`='ERU'), 2) AS price_eur,
                                        p.product_details,
                                        p.category_id
                                    FROM
                                        sma_products p
                                        
                                        WHERE {$where}
                                    ORDER BY p.id DESC");

        return $query->result();
    }

    public function brand()
    {
        $query = $this->db->query("SELECT id,name,code from sma_brands ORDER BY id DESC");
        return $query->result();
    }

    public function product_category($paramater)
    {
        $category_id = $paramater['category_id'];
        $subcategory_id = $paramater['subcategory_id'];

        $customer_id = $paramater['customer_id'];
        $ifelse = $paramater['ifelse'];
        $brand_id = $paramater['brand_id'];
        $limit = $paramater['limit'];
        $offset = $paramater['offset'];
        $sql_limit = "";
        if ($limit > 0 && $offset != "") {
            $page = ($offset - 1) * $limit;
            $sql_limit = "LIMIT $page,$limit";
        }

        $path = base_url('assets/uploads/');
        $where = "";
        if ($brand_id != 0) {
            $where .= " AND p.brand = '$brand_id' ";
        }

        if ($subcategory_id != 0) {
            $where .= "AND p.category_id in(SELECT id FROM sma_categories WHERE parent_id ='$category_id' AND id = '$subcategory_id')";

        } else {

            if ($ifelse == null) {
                $where .= "AND p.category_id ='$category_id'";
            }
            else {
                $where .= "AND p.category_id in(SELECT id FROM sma_categories WHERE parent_id ='$category_id')";
            }

        }
        $query = $this->db->query("SELECT
                                        p.id,
                                        p.code,
                                        CONCAT('$path',p.image) as image,
                                        p.name,
                                        p.brand,
                                        ROUND(p.price , 2) AS price_usa,
                                        ROUND(p.price  * (SELECT rate FROM sma_currencies WHERE `code`='REL'), 0) AS price_rei,
                                        ROUND(p.price  * (SELECT rate FROM sma_currencies WHERE `code`='ERU'), 2) AS price_eur,
                                        p.product_details,
                                        p.category_id,
                                        IF(f.id,'true','false') as favorite
                                        FROM
                                        sma_products p
                                        LEFT JOIN sma_favorite f
                                        ON f.product_id = p.id AND f.customer_id = 'null'
                                        WHERE 1=1 {$where}
                                        ORDER BY p.id DESC
                                        {$sql_limit}
                                        ");
        return $query->result();

    }


//    list product by category and customer

    public function product_category_customer($paramater)
    {
        $category_id = $paramater['category_id'];
        $subcategory_id = $paramater['subcategory_id'];

        $customer_id = $paramater['customer_id'];
        $ifelse = $paramater['ifelse'];
        $brand_id = $paramater['brand_id'];
        $limit = $paramater['limit'];
        $offset = $paramater['offset'];
        $sql_limit = "";
        if ($limit > 0 && $offset != "") {
            $page = ($offset - 1) * $limit;
            $sql_limit = "LIMIT $page,$limit";
        }

        $path = base_url('assets/uploads/');
        $where = "";
        if ($brand_id != 0) {
            $where .= " AND p.brand = '$brand_id' ";
        }

        if ($subcategory_id != 0) {
            $where .= "AND p.category_id in(SELECT id FROM sma_categories WHERE parent_id ='$category_id' AND id = '$subcategory_id')";

        } else {

            if ($ifelse == null) {
                $where .= "AND p.category_id ='$category_id'";
            }
            else {
                $where .= "AND p.category_id in(SELECT id FROM sma_categories WHERE parent_id ='$category_id')";
            }

        }

        $query = $this->db->query("SELECT
                                        p.id,
                                        p.code,
                                        CONCAT('$path',p.image) as image,
                                        p.name,
                                        p.brand,
                                        ROUND(p.price , 2) AS price_usa,
                                        ROUND(p.price  * (SELECT rate FROM sma_currencies WHERE `code`='REL'), 0) AS price_rei,
                                        ROUND(p.price  * (SELECT rate FROM sma_currencies WHERE `code`='ERU'), 2) AS price_eur,
                                        p.product_details,
                                        p.category_id,
                                        IF(f.id,'true','false') as favorite
                                        FROM
                                        sma_products p
                                        LEFT JOIN sma_favorite f
                                        ON f.product_id = p.id AND f.customer_id = '$customer_id'
                                        WHERE 1=1 {$where}
                                        ORDER BY p.id DESC 
                                        {$sql_limit}
                                ");

        return $query->result();
    }


    public function list_brand()
    {
        $qurey = $this->db->query("
        SELECT
        *
        FROM
        sma_brands
        ");
        return $qurey->result();
    }


    public function if_fun($category_id){
        $query = $this->db->query("
                                    SELECT
                                    c.id as id
                                    FROM
                                    sma_categories c
                                    WHERE
                                    (c.parent_id !='' OR c.parent_id !=0 OR c.parent_id != null)
                                    AND c.parent_id = '$category_id'
                                    ORDER BY c.id DESC
                                  ")->result();
        return $query;
    }

    function getbrand_bycategory($paramater)
    {


        $category_id = $paramater['category_id'];
        $subcategory_id = $paramater['subcategory_id'];
        $ifelse = $paramater['ifelse'];

        $where = '';

        if ($ifelse == null) {
            $where .= "AND p.subcategory_id in(SELECT id from sma_categories where parent_id='' OR parent_id= null OR parent_id= 0) OR p.category_id in(SELECT id from sma_categories where parent_id='' OR parent_id= null OR parent_id= 0)";
        }
        else {
            $where .= "AND p.subcategory_id in(SELECT id from sma_categories where parent_id='$category_id') OR p.category_id in(SELECT id from sma_categories where parent_id='$category_id')";
        }
        if ($subcategory_id != 0) {
            $where .= "AND p.category_id in(SELECT id FROM sma_categories WHERE parent_id ='$category_id' AND id = '$subcategory_id')";
        }
        else {
            $where .= "AND p.category_id in(SELECT id from sma_categories where id='$category_id') OR p.category_id in(SELECT id from sma_categories where parent_id='$category_id')";

        }

            $brand = $this->db->query("SELECT
                                        b.*
                                        FROM 
                                        sma_products p 
                                        INNER JOIN 
                                        sma_brands b 
                                        ON p.brand=b.id
                                        WHERE 1=1
                                        {$where} 
                                        GROUP BY b.id
                                        ORDER BY b.id DESC 
                                  ")->result();

            $data[0] = (object)[
                'id' => 0,
                'code' => '0',
                'name' => 'All',
                'image' => '',
                'slug' => ''
            ];
            foreach ($brand as $b) {
                array_push($data, $b);
            }
            return $data;

        }


        public function check_product_id($product_id, $customer_id)
        {
            $query = $this->db->query("
                                    SELECT
                                    *
                                    FROM
                                    sma_cart
                                    WHERE
                                    product_id = '" . $product_id . "' 
                                    AND
                                    customer_id= '" . $customer_id . "'
                                    ");
            return $query->row();
        }


        public
        function update_qty($qty, $product_id, $customer_id)
        {
            $update_qty = [
                'customer_id' => $customer_id,
                'product_id' => $product_id,
                'qty' => $qty,
                'date' => date("Y-m-d H:i:s"),
            ];
            if ($this->db->update('sma_cart', $update_qty, array('product_id' => $product_id, 'customer_id' => $customer_id))) {
                $query = $this->db->query("select * from sma_cart where product_id= '$product_id' and customer_id = '$customer_id'");
                return $query->row();
            } else {
                $output = ['status' => 'fail', 'msg' => 'Data is not successfully.', 'update_cart' => []];
                return $output;
            }
        }


        public function save_cart($data)
        {
            $save = [
                'customer_id' => $data['customer_id'],
                'product_id' => $data['product_id'],
                'qty' => $data['qty'],
                'date' => date("Y-m-d H:i:s"),
            ];
            if ($this->db->insert('sma_cart', $save)) {
                $id = $this->db->insert_id();
                $query = $this->db->query("select * from sma_cart where id= '$id'");
                return $query->row();
            } else {
                $output = ['status' => 'fail', 'msg' => 'Data is not successfully.', 'store_cart' => []];
                return $output;
            }
        }


        public
        function list_cart($id)
        {
            $path = base_url('assets/uploads/');
            $query = $this->db->query("SELECT 
                                        s.id,
                                        p.id as proid,
                                        p.code,
                                        s.customer_id, 
                                        s.qty,
                                        CONCAT('$path',p.image) as image,
                                        p.name,
                                        ROUND(p.price , 2) AS price_usa,
                                        ROUND(p.price  * (SELECT rate FROM sma_currencies WHERE `code`='REL'), 0) AS price_rei,
                                        ROUND(p.price  * (SELECT rate FROM sma_currencies WHERE `code`='ERU'), 2) AS price_eur,
                                        p.product_details,
                                        p.category_id,
                                        IF(f.id,'true','false') as favorite
                                        FROM sma_products p
                                        INNER JOIN sma_cart s
                                        ON p.id=s.product_id
                                        LEFT JOIN sma_favorite f
                                        ON f.product_id=p.id  AND f.customer_id = '$id'
                                        WHERE s.customer_id = '$id'
                                        ORDER BY s.id DESC
                                    ");
            return $query->result();
        }


        public
        function count_row($id)
        {
            $query = $this->db->query("SELECT * FROM sma_cart WHERE customer_id = '$id'");
            return $query->num_rows();
        }


        public
        function total_price($id)
        {
            $query = $this->db->query("SELECT 
                                        IFNULL(sum(p.price * sp.qty),0) AS price_usa,
                                        REPLACE (FORMAT (IFNULL( sum( ( p.price * ( SELECT rate FROM sma_currencies WHERE `code` = 'REL' ) ) * sp.qty ), 0 ),0),',','') AS price_rei
                                        FROM sma_products p
                                        INNER JOIN sma_cart sp
                                        ON p.id=sp.product_id
                                        WHERE
                                        sp.customer_id = '$id'
                                    ");
            return $query->row();
        }


        public
        function find_id($id)
        {
            $path = base_url('assets/uploads/');
            $where = "";
            if ($id) {
                $where .= "sma_cart.id = $id";
            }
            $query = $this->db->query("SELECT 
                                        CONCAT('" . $path . "',sma_products.image) as image,
                                        ROUND(sma_products.price , 2) AS price_usa,
                                        ROUND(sma_products.price  * (SELECT rate FROM sma_currencies WHERE `code`='REL'), 2) AS price_rei,
                                        sma_products.name,
                                        sma_products.id as product_id,
                                        sma_products.product_details,
                                        sma_cart.id, 
                                        sma_cart.customer_id, 
                                        sma_cart.qty
                                        FROM sma_products
                                        INNER JOIN sma_cart ON sma_products.id=sma_cart.product_id WHERE {$where}
                                    ");
            return $query->result();
            return $query->row();
        }


        public
        function update_cart($data, $id)
        {

            $update_cart = [
                'customer_id' => $data['customer_id'],
                'product_id' => $data['product_id'],
                'qty' => $data['qty'],
                'date' => date("Y-m-d H:i:s"),
            ];

            if ($this->db->update('sma_cart', $update_cart, array('id' => $id))) {
                $query = $this->db->query("select * from sma_cart where id= '$id'");
                return $query->row();
            } else {
                $output = ['status' => 'fail', 'msg' => 'Data is not successfully.', 'update_cart' => []];
                return $output;
            }

        }

        public
        function delete_cart($id)
        {

            $query = $this->db->query("select * from sma_cart where id= '$id'")->row();
            if ($query) {
                $this->db->delete('sma_cart', array('id' => $id));
                return true;
            } else {
                return false;
            }
        }

        public
        function get_provinces()
        {
            $query = $this->db->query("select id,name from provinces");

            return $query->result();
        }

        public
        function get_districts($id)
        {
            $where = "";
            if ($id) {
                $where .= "province_id = '$id'";
            }
            $query = $this->db->query("SELECT id,name FROM districts WHERE {$where}");

            return $query->result();
        }

        public
        function get_communes($id)
        {
            $where = "";
            if ($id) {
                $where .= "district_id = '$id'";
            }
            $query = $this->db->query("SELECT id,name FROM communes WHERE {$where}");

            return $query->result();
        }

        public
        function get_villages($id)
        {
            $where = "";
            if ($id) {
                $where .= "commune_id = '$id'";
            }
            $query = $this->db->query("SELECT id,name FROM villages WHERE {$where}");

            return $query->result();
        }

        public
        function save_address($data)
        {
            $save = [
                'sale_id' => $data['sale_id'],
                'provinces' => $data['provinces'],
                'districts' => $data['districts'],
                'communes' => $data['communes'],
                'villages' => $data['villages'],
                'address' => $data['address'],
                'lat' => $data['lat'],
                'long' => $data['long'],
            ];

            if ($this->db->insert('sma_cart_delivery', $save)) {
                $output = ['status' => 'true', 'msg' => 'Data is inserted successfully.'];
                return $output;
            } else {
                $output = ['status' => 'fail', 'msg' => 'Data is not successfully.'];
                return $output;
            }
        }

        public
        function save_favorite($data)
        {
            $path = base_url('assets/uploads/');
            $save = [
                'customer_id' => $data['customer_id'],
                'product_id' => $data['product_id'],
                'date' => date("Y-m-d H:i:s"),
            ];
            if ($this->db->insert('sma_favorite', $save)) {
                $id = $this->db->insert_id();
                $query = $this->db->query("SELECT 
                                        f.id,
                                        f.customer_id,
                                        p.id as product_id,
                                        p.name,
                                        CONCAT('$path',p.image) as image,
                                        ROUND(p.price , 2) AS price_usa,
                                        ROUND(p.price  * (SELECT rate FROM sma_currencies WHERE `code`='REL'), 2) AS price_rei
                                        FROM 
                                        sma_products p
                                        INNER JOIN 
                                        sma_favorite f
                                        ON f.product_id=p.id 
                                        WHERE
                                         f.id = '$id'
                                         ");
                return $query->row();
            } else {
                $output = ['status' => 'fail', 'msg' => 'Data is not successfully.', 'add_favorite' => []];
                return $output;
            }
        }

        public
        function sql_favorite($customer_id, $limit, $offset)
        {

            $sql_limit = "";
            if ($limit > 0 && $offset != "") {
                $page = ($offset - 1) * $limit;
                $sql_limit = "LIMIT $page,$limit";
            }

            $path = base_url('assets/uploads/');

            $where = "";
            if ($customer_id) {
                $where .= "customer_id = '$customer_id'";
            }
            $query = $this->db->query("SELECT 
                                        p.id,
                                        p.code,
                                        CONCAT('$path',p.image) as image,
                                        p.name,
                                        p.brand,
                                        ROUND(p.price , 2) AS price_usa,
                                        ROUND(p.price  * (SELECT rate FROM sma_currencies WHERE `code`='REL'), 0) AS price_rei,
                                        ROUND(p.price  * (SELECT rate FROM sma_currencies WHERE `code`='ERU'), 2) AS price_eur,
                                        p.product_details,
                                        p.category_id,
                                        CONCAT('true') as favorite
                                        FROM sma_products p
                                        INNER JOIN sma_favorite f
                                        ON f.product_id=p.id WHERE {$where} {$sql_limit}
                                    ");
            return $query->result();
        }


        public
        function if_favorite($customer_id, $product_id)
        {
            $query = $this->db->query("SELECT
                                        * FROM sma_favorite
                                        WHERE customer_id = '$customer_id' AND product_id = '$product_id'
                                    ");
            return $query->row()->id;
        }


        public
        function delete_favorite($id)
        {
            $query = $this->db->query("select * from sma_favorite where id= '$id'")->row();
            if ($query) {
                $this->db->delete('sma_favorite', array('id' => $id));
                return true;
            } else {
                return false;
            }
        }

        public
        function fun_search($paramater)
        {
            $path = base_url('assets/uploads/');
            $keysearch = $paramater['keysearch'];
            $customer_id = $paramater['customer_id'];
            $limit = $paramater['limit'];
            $offset = $paramater['offset'];
            $sql_limit = "";
            if ($limit > 0 && $offset != "") {
                $page = ($offset - 1) * $limit;
                $sql_limit = "LIMIT $page,$limit";
            }
            if ($customer_id) {
                $query = $this->db->query("SELECT
                                            p.id,
                                            p.code,
                                            CONCAT('$path',p.image) as image,
                                            p.name,
                                            p.brand,
                                            ROUND(p.price , 2) AS price_usa,
                                            ROUND(p.price  * (SELECT rate FROM sma_currencies WHERE `code`='REL'), 0) AS price_rei,
                                            ROUND(p.price  * (SELECT rate FROM sma_currencies WHERE `code`='ERU'), 2) AS price_eur,
                                            p.product_details,
                                            p.category_id,
                                            IF(f.id,'true','false') as favorite
                                            FROM 
                                            sma_products p
                                            LEFT JOIN sma_favorite f
                                            ON f.product_id=p.id  AND f.customer_id = $customer_id
                                            WHERE p.name LIKE '%{$keysearch}%'
                                            {$sql_limit}
                                      ");
                return $query->result();
            } else {
                $query = $this->db->query("SELECT
                                        p.id,
                                        p.code,
                                        CONCAT('$path',p.image) as image,
                                        p.name,
                                        p.brand,
                                        ROUND(p.price , 2) AS price_usa,
                                        ROUND(p.price  * (SELECT rate FROM sma_currencies WHERE `code`='REL'), 0) AS price_rei,
                                        ROUND(p.price  * (SELECT rate FROM sma_currencies WHERE `code`='ERU'), 2) AS price_eur,
                                        p.product_details,
                                        p.category_id,
                                        IF(f.id,'true','false') as favorite
                                        FROM
                                        sma_products p
                                        LEFT JOIN sma_favorite f
                                        ON f.product_id = p.id AND f.customer_id = 'hf'
                                        WHERE p.name LIKE '%{$keysearch}%'
                                        {$sql_limit}
                                        ")->result();
                return $query;
            }
        }

    }