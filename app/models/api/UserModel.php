<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class UserModel extends CI_Model
{


    public function checkEmail($email)
    {
        $where = "";
        if ($email) {
            $where .= "email = '$email' OR phone = '$email";
        }
        $query = $this->db->query("SELECT * FROM sma_companies WHERE {$where}");
        return $query->num_rows();
    }


    public function insert($data)
    {
        $register = [
            'name' => $data['first_name'] .' '. $data['last_name'],
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'email' => $data['email'],
            'password' => $data['password'],
            'phone' => $data['phone'],
            'image' => $data['image'],
            'group_id' => 3,
            'group_name' => 'customer',
        ];

        if ($this->db->insert('sma_companies', $register)) {
            $output = ['status' => 'true', 'msg' => 'Data is inserted successfully.', $register];
            return $output;

        } else {
            $output = ['status' => 'fail', 'msg' => 'Data is not successfully.'];
            return $output;

        }
    }

}