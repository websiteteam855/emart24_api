<html>
<head>
    <link href="<?php echo base_url('themes/default/admin/assets/styles/helpers/bootstrap.min.css') ?>"
          rel="stylesheet">
    <script src="<?php echo base_url('themes/default/admin/assets/js/jquery.js') ?>"></script>
    <style>
        .description_kh{
            margin-bottom: 30px;
        }
        .title{
            margin-top: 15px;
            margin-bottom: 15px;
            font-weight: bold;
        }
    </style>
</head>
<body>
<div class="container">
    <?php foreach ($data as $key => $item):?>

        <div class="<?php echo $key;?>"><?php echo $item;?></div>

    <?php endforeach;?>
</div>
</body>

</html>