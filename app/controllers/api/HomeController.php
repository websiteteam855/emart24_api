<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class HomeController extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        header('Content-Type: application/json');
        $this->load->model('api/HomeModel', 'home_api');

    }

    public function index()
    {


        $id = $this->input->get('id');
        $customer_id = $this->input->get('customer_id');
        $limit = $this->input->get('limit');
        $offset = $this->input->get('offset');

        //product detail by customer
        if ($id && $customer_id) {
            $product_detail = $this->home_api->product_detail_customer($id, $customer_id);
            if ($product_detail == NULL) {
                $output = ['status' => 'false', 'msg' => 'data not found.', 'product_detail' => []];
                echo json_encode($output);
            } else {
                echo json_encode(['product_detail' => $product_detail]);
            }

        } //product detail
        elseif ($id) {
            $product_detail = $this->home_api->product_detail($id);
            if ($product_detail == NULL) {
                $output = ['status' => 'false', 'msg' => 'data not found.', 'product_detail' => []];

                echo json_encode($output);
            } else {
                echo json_encode(['product_detail' => $product_detail]);
            }

        } //product lists by customer
        elseif ($customer_id) {
            $product_favorites = $this->home_api->product_get_customer($customer_id, $limit, $offset);
            if ($product_favorites == NULL) {
                $output = ['status' => 'false', 'msg' => 'data not found.', 'list_product' => []];
                echo json_encode($output);
            } else {
                $count_item = $this->home_api->count_row($customer_id);
                $total_price = $this->home_api->total_price($customer_id);
                echo json_encode(['count_item' => $count_item, 'total_price' => $total_price, 'list_product' => $product_favorites]);
            }

        } //product lists by customer
        else {
            $products = $this->home_api->product_get($limit, $offset);
            if ($products == NULL) {
                $output = ['status' => 'false', 'msg' => 'data not found.', 'list_product' => []];
                echo json_encode($output);
            } else {
                $output = ['price_usa' => 0, 'price_rei' => 0];
                echo json_encode(['count_item' => 0, 'total_price' => $output, 'list_product' => $products]);
            }
        }
    }


//------- new 27. 02. 2019  --------/
    public function counttest()
    {
        $id = $this->input->get('id');
        // product_detail
        if ($id != null) {
            $count_item = $this->home_api->count_row($id);
            $total_price = $this->home_api->total_price($id);
            if ($count_item == NULL) {
                $output = ['status' => 'false', 'msg' => 'data not found.', 'count_cart' => []];

                echo json_encode($output);
            } else {
                echo json_encode(['customer_id' => $id, 'count_item' => $count_item, 'total_price' => $total_price]);
            }

        }

    }


    public function total_cart()
    {
        $id = $this->input->get('id');

        if ($id != null) {
            $count_item = $this->home_api->count_row($id);
            $total_price = $this->home_api->total_price($id);
            if ($count_item == NULL) {
                $output = ['status' => 'false', 'msg' => 'data not found.', 'count_cart' => []];

                echo json_encode($output);
            } else {
                echo json_encode(['customer_id' => $id, 'count_item' => $count_item, 'total_price' => $total_price]);
            }

        }

    }


    public function banner()
    {
        $banner = $this->home_api->get_banner();
        if ($banner == NULL) {
            $output = ['status' => 'false', 'msg' => 'data not found.', 'list_banner' => []];
            echo json_encode($output);
        } else {
            echo json_encode(['list_banner' => $banner]);
        }

    }

    public function brand()
    {
        $data = $this->home_api->list_brand();
        $msg = "list_brand";
        if ($data) {
            echo json_encode([$msg => $data]);
        } else {
            echo json_encode([$msg => []]);
        }
    }

    public function contact_us()
    {
        $contact_us['data'] = $this->home_api->get_contact_us();
        if ($contact_us == NULL) {
            $output = ['status' => 'false', 'msg' => 'data not found.', 'contact_us' => []];
            echo json_encode($output);
        } else {
            $apply_online = $this->load->view($this->theme . 'api/contact.php', $contact_us, true);
            $rt = array('contact_us' => $apply_online);
            echo json_encode($rt);
        }
    }

    public function delivery() {
        $data['content'] = $this->home_api->delivery_price();
        if ($data == NULL) {
            $output = ['delivery' => []];
            echo json_encode($output);
        } else {
            $apply_online = $this->load->view($this->theme . 'api/deliveryprice.php', $data, true);
            $rt = array('delivery' => $apply_online);
            echo json_encode($rt);
        }
    }

    public function about_us()
    {
        $data['data'] = $this->home_api->get_about_us();
        if ($data == NULL) {
            $output = ['status' => 'false', 'msg' => 'data not found.', 'about_us' => []];
            echo json_encode($output);
        } else {
            $apply_online = $this->load->view($this->theme . 'api/about.php', $data, true);
            $rt = array('about_us' => $apply_online);
            echo json_encode($rt);
        }
    }

    function get_category()
    {
        $get_category = $this->home_api->main_category();
        if ($get_category == NULL) {
            $output = ['status' => 'false', 'msg' => 'data not found.', 'main_category' => []];
            echo json_encode($output);
        } else {
            echo json_encode(['main_category' => $get_category]);
        }
    }

    function get_subcategory()
    {
        $category = $this->input->get('category_id');
        $sub_category = $this->home_api->sub_category($category);
        if ($sub_category == NULL) {
            $output = ['status' => 'false', 'msg' => 'data not found.', 'sub_category' => []];
            echo json_encode($output);
        } else {
            echo json_encode(['sub_category' => $sub_category]);
        }
    }

    function get_brand()
    {
        $category_id = $this->input->get('category_id');
        $subcategory_id = $this->input->get('subcategory_id');

        $ifelse = $this->home_api->if_fun($category_id);
        $paramater = [
            'category_id' => $category_id,
            'subcategory_id' => $subcategory_id,
            'ifelse' => $ifelse
         ];
        $brand = $this->home_api->getbrand_bycategory($paramater);
        echo json_encode(['brand' => $brand]);
    }

    public function get_query()
    {
        $msg = "get_query";
        $sqlquery = $this->input->post('query');
        $data = $this->home_api->list_query($sqlquery);
        if ($data) {
            echo json_encode([$msg => $data]);
        } else {
            echo json_encode([$msg => false]);
        }
    }

    function get_product()
    {
        $category_id = $this->input->get('category_id');
        $subcategory_id = $this->input->get('subcategory_id');
        $customer_id = $this->input->get('customer_id');
        $brand_id = $this->input->get('brand_id');
        $limit = $this->input->get('limit');
        $offset = $this->input->get('offset');
        $ifelse = $this->home_api->if_fun($category_id);
        $paramater = [
            'category_id' => $category_id,
            'subcategory_id' => $subcategory_id,
            'customer_id' => $customer_id,
            'brand_id' => $brand_id,
            'limit' => $limit,
            'offset' => $offset,
            'ifelse' => $ifelse,
        ];

        if ($customer_id) {
            $products = $this->home_api->product_category_customer($paramater);
            if ($products == NULL) {
                $id = $customer_id;
                $count_item = $this->home_api->count_row($id);
                $total_price = $this->home_api->total_price($id);
                echo json_encode(['count_item' => $count_item, 'total_price' => $total_price,'status' => 'false', 'msg' => 'data not found.', 'list_product' => []]);

            } else {
                $count_item = $this->home_api->count_row($customer_id);
                $total_price = $this->home_api->total_price($customer_id);
                echo json_encode(['count_item' => $count_item, 'total_price' => $total_price, 'list_product' => $products]);
            }
        } else {
            $products = $this->home_api->product_category($paramater);
            if ($products == NULL) {
                $id = "";
                $count_item = $this->home_api->count_row($id);
                $total_price = $this->home_api->total_price($id);
                echo json_encode(['count_item' => $count_item, 'total_price' => $total_price, 'status' => 'false', 'msg' => 'data not found.','list_product' => []]);
            } else {
                $id = "";
                $count_item = $this->home_api->count_row($id);
                $total_price = $this->home_api->total_price($id);
                echo json_encode(['count_item' => $count_item, 'total_price' => $total_price, 'list_product' => $products]);
            }
        }
    }

    public function store_cart()
    {
        $data = array(
            'customer_id' => $this->input->post('customer_id'),
            'product_id' => $this->input->post('product_id'),
            'qty' => $this->input->post('qty'),
        );
        $customer_id = $data['customer_id'];

        $product = $this->home_api->check_product_id($data['product_id'], $customer_id);
        if ($product->product_id == $data['product_id'] && $product->customer_id == $data['customer_id']) {
            $qty = $product->qty;
            $re = $this->home_api->update_qty($qty + 1, $data['product_id'], $customer_id);
            $count_item = $this->home_api->count_row($customer_id);
            $total_price = $this->home_api->total_price($customer_id);
        } else {
            $re = $this->home_api->save_cart($data);
            $count_item = $this->home_api->count_row($customer_id);
            $total_price = $this->home_api->total_price($customer_id);
        }


        echo json_encode(['store_cart' => $re, 'count_item' => $count_item, 'total_price' => $total_price]);


    }

    public function view_cart()
    {
        $id = $this->input->get('customer_id');
        $view_cart = $this->home_api->list_cart($id);
        $count_item = $this->home_api->count_row($id);
        $total_price = $this->home_api->total_price($id);
        if ($view_cart == NULL) {
            $output = ['status' => 'false', 'msg' => 'data not found.', 'view_cart' => []];
            echo json_encode($output);
        } else {
            echo json_encode(['count_item' => $count_item, 'total_price' => $total_price, 'view_cart' => $view_cart]);
        }

    }

    public function find_cart()
    {
        $id = $this->input->get('id');
        $re = $this->home_api->find_id($id);
        echo json_encode(['find_cart' => $re]);

    }

    public function update()
    {
        $id = $this->input->get('id');
        $data = array(
            'customer_id' => $this->input->post('customer_id'),
            'product_id' => $this->input->post('product_id'),
            'qty' => $this->input->post('qty'),
        );
        $customer_id = $data['customer_id'];
        $re = $this->home_api->update_cart($data, $id);
        $count_item = $this->home_api->count_row($customer_id);
        $total_price = $this->home_api->total_price($customer_id);
        echo json_encode(['update' => $re, 'count_item' => $count_item, 'total_price' => $total_price]);

    }

    public function delete()
    {
        $id = $this->input->get('id');
        $customer_id = $this->input->get('customer_id');

        $re = $this->home_api->delete_cart($id);
        $count_item = $this->home_api->count_row($customer_id);
        $total_price = $this->home_api->total_price($customer_id);

        echo json_encode(['delete_cart' => $re, 'count_item' => $count_item, 'total_price' => $total_price]);

    }

    public function provinces()
    {
        $provinces = $this->home_api->get_provinces();
        if ($provinces == NULL) {
            $output = ['status' => 'false', 'msg' => 'data not found.', 'provinces' => []];
            echo json_encode($output);
        } else {
            echo json_encode(['provinces' => $provinces]);
        }


    }

    public function districts()
    {
        $id = $this->input->get('id');
        $districts = $this->home_api->get_districts($id);
        if ($districts == NULL) {
            $output = ['status' => 'false', 'msg' => 'data not found.', 'districts' => []];
            echo json_encode($output);
        } else {
            echo json_encode(['districts' => $districts]);
        }

    }

    public function communes()
    {
        $id = $this->input->get('id');
        $communes = $this->home_api->get_communes($id);
        if ($communes == NULL) {
            $output = ['status' => 'false', 'msg' => 'data not found.', 'communes' => []];
            echo json_encode($output);
        } else {
            echo json_encode(['communes' => $communes]);
        }

    }

    public function villages()
    {
        $id = $this->input->get('id');
        $villages = $this->home_api->get_villages($id);
        if ($villages == NULL) {
            $output = ['status' => 'false', 'msg' => 'data not found.', 'villages' => []];
            echo json_encode($output);
        } else {
            echo json_encode(['villages' => $villages]);
        }

    }

    public function favorite()
    {
        $data = array(
            'customer_id' => $this->input->post('customer_id'),
            'product_id' => $this->input->post('product_id'),
        );

        $id = $this->home_api->if_favorite($data['customer_id'], $data['product_id']);
        if ($id) {
            $del = $this->home_api->delete_favorite($id);
            if ($del) {
                echo json_encode(['isfavorite' => false]);
            }

        } else {
            $favorite = $this->home_api->save_favorite($data);
            echo json_encode(['isfavorite' => true, 'favorite' => $favorite]);
        }
    }

    public function list_favorite()
    {
        $customer_id = $this->input->get('customer_id');
        $limit = $this->input->get('limit');
        $offset = $this->input->get('offset');
        $favorite = $this->home_api->sql_favorite($customer_id, $limit, $offset);
        $count_item = $this->home_api->count_row($customer_id);
        $total_price = $this->home_api->total_price($customer_id);
        echo json_encode(['count_item' => $count_item, 'total_price' => $total_price, 'list_product' => $favorite]);
    }

    public function delete_favorite()
    {
        $id = $this->input->get('id');
        $favorite = $this->home_api->delete_favorite($id);
        echo json_encode(['delete_favorite' => $favorite]);
    }
    
    
     public function search() {
        $keysearch = $this->input->get('keysearch');
        $customer_id = $this->input->get('customer_id');
        $limit = $this->input->get('limit');
        $offset = $this->input->get('offset');
        $paramater = [
            'keysearch' => $keysearch,
            'customer_id' => $customer_id,
            'limit' => $limit,
            'offset' => $offset
        ];
        $msg = "list_product";
        $data = $this->home_api->fun_search($paramater);
        if ($data == NULL) {
            $count_item = $this->home_api->count_row($customer_id);
            $total_price = $this->home_api->total_price($customer_id);
            $output = ['count_item' => $count_item, 'total_price' => $total_price, $msg => []];
            echo json_encode($output);
        } else {
            $count_item = $this->home_api->count_row($customer_id);
            $total_price = $this->home_api->total_price($customer_id);
            $output = ['count_item' => $count_item, 'total_price' => $total_price, $msg => $data];
            echo json_encode($output);
        }

    }

}