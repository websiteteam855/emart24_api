<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class LoginController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        header('Content-Type: application/json');

        $this->load->model('api/LoginModel', 'login');
    }

    public function login(){

        $email =   $this->input->get('email');
        $pass =    $this->input->get('password');
        $query = $this->login->login_api($email,md5($pass));
        if($query == NULL)
        {
            $output = ['status' => 'false','msg' => 'check email and password.'];
            echo json_encode(['message'=>$output]);

        }else{
            $output = ['status' => 'true','msg' => 'login successful.'];
            echo json_encode(['message'=>$output, 'get_login'=>$query]);
        }
    }
}
?>