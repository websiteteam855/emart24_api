<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');


class UserController extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        header('Content-Type: application/json');

        $this->load->model('api/UserModel', 'register');
    }


    

    public function register()
    {

        if ($_FILES['image']['size'] > 0) {
            $this->load->library('upload');
            $config['upload_path'] = 'assets/uploads/register/';
            $config['allowed_types'] = "gif|jpg|jpeg|png|tif";
            $config['max_size'] = 0;
//            $config['max_width'] = 300;
//            $config['max_height'] = 80;
            $config['overwrite'] = TRUE;
            $config['encrypt_name'] = TRUE;
            $this->upload->initialize($config);
            if (!$this->upload->do_upload('image')) {
                $error = $this->upload->display_errors();
                echo json_encode($error);
                die();
            }
            $image = $this->upload->file_name;

        }

        $data = array(
            'first_name' => $this->input->post('first_name'),
            'last_name' => $this->input->post('last_name'),
            'email' => $this->input->post('email'),
            'password' => md5($this->input->post('password')),
            'phone' => $this->input->post('phone'),
            'image' => $image,
        );

        if ($data == null) {
            $output = ['data' => 'fail', 'msg' => 'data not found.'];
            echo json_encode($output);
        } else {

            $re = $this->register->insert($data);

            echo json_encode(['register' => $re]);
        }

    }


}