<?php defined('BASEPATH') OR exit('No direct script access allowed');

class CheckoutModel extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        header('Content-Type: application/json');
        $this->load->model('api/CurrenciesFormat', 'format');
    }

    public function curr_rate() {
        $curr = $this->db->query("
        SELECT
        rate
        FROM
        sma_currencies
        WHERE
        `code` = 'REL'
        ")->row();
        return $curr;
    }
    public function save_checkout($customer_id, $data)
    {
        $save = [
            'date' => date("Y-m-d H:i:s"),
            'reference_no' => rand(100000, 9999999),
            'customer_id' => $customer_id,
            'customer' => $data['customer'],
            'biller_id' => '',
            'biller' => '',
            'warehouse_id' => '',
            'note' => '',
            'staff_note' => '',
            'total' => $data['total'],
            'product_discount' => $data['product_discount'],
            'order_discount_id' => $data['order_discount_id'],
            'total_discount' => $data['total_discount'],
            'order_discount' => $data['order_discount'],
            'product_tax' => $data['product_tax'],
            'order_tax_id' => $data['order_tax_id'],
            'order_tax' => $data['order_tax'],
            'total_tax' => $data['total_tax'],
            'shipping' => $data['shipping'],
            'grand_total' => $data['grand_total'],
            'sale_status' => 'Pending',
            'payment_status' => 'Pending',
            'payment_term' => '',
            'due_date' => '',
            'created_by' => '',
            'updated_by' => '',
            'updated_at' => '',
            'total_items' => '',
            'pos' => '',
            'paid' => '',
            'return_id' => '',
            'surcharge' => '',
            'attachment' => '',
            'return_sale_ref' => '',
            'rounding' => '',
            'suspend_note' => '',
            'api' => 1,
            'shop' => '',
            'address_id' => '',
            'reserve_id' => '',
            'hash' => hash('sha256', microtime() . mt_rand()),
            'exchange_rate' => '',
            'is_pub_tax' => '',
            'old_id' => '',
        ];
        if ($this->db->insert('sma_sales', $save)) {
            $id = $this->db->insert_id();

            $query = $this->db->query("select * from sma_sales where id= '$id'");
            $query->row();
            return $query->row();
        } else {
            $output = ['status' => 'fail', 'msg' => 'Data is not successfully.'];
            return $output;
        }
    }

    public function sale_items($sale_items)
    {
        if ($this->db->insert('sma_sale_items', $sale_items)) {
            return $sale_items;
        } else {
            $output = ['status' => 'fail', 'msg' => 'Data is not successfully.'];
            return $output;
        }
    }


    public function update_sale($total, $re)
    {
        $update_sale = [
            'total' => $total,
            'grand_total' => $total,
        ];
        $this->db->update('sma_sales', $update_sale, array('id' => $re->id));
    }

    public function deliveries($deliveries)
    {
        $save = [
            'sale_id' => $deliveries['sale_id'],
            'customer_id' => $deliveries['customer_id'],
            'provinces' => $deliveries['provinces'],
            'districts' => $deliveries['districts'],
            'communes' => $deliveries['communes'],
            'villages' => $deliveries['villages'],
            'address' => $deliveries['address'],
            'lat' => $deliveries['lat'],
            'long' => $deliveries['long'],
            'email' => $deliveries['email'],
            'phone' => $deliveries['phone'],
        ];
        $this->db->insert('sma_cart_delivery', $save);
        $id = $this->db->insert_id();
        $query = $this->db->query("
                                    SELECT
                                    scd.id,
                                    scd.sale_id,
                                    cp.first_name,
                                    cp.last_name,
                                    scd.address,
                                    scd.email,
                                    scd.phone,
                                    p.name AS provinces,
                                    d.name AS districts,
                                    c.name AS communes,
                                    scd.villages,
                                    scd.lat,
                                    scd.long
                                    FROM
                                    sma_cart_delivery scd
                                    INNER JOIN 
                                    provinces p
                                    ON scd.provinces = p.id
                                    INNER JOIN 
                                    districts d
                                    ON scd.districts = d.id
                                    INNER JOIN 
                                    communes c
                                    ON scd.communes = c.id
                                    INNER JOIN
                                    sma_companies cp
                                    ON scd.customer_id = cp.id
                                    WHERE
                                    scd.id = '$id'
     ")->row();
        return $query;
    }

    public function getProductNames($id)
    {
        $q = $this->db->get_where('sma_products', array('id' => $id));
        if ($q->num_rows() > 0) {
            return $q->row();
        }


        return FALSE;
    }

    public function getCompanyByID($id)
    {
        $q = $this->db->get_where('sma_companies', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }

        return FALSE;
    }

    public function min_id()
    {
        $id = $this->db->query("SELECT MIN(id) AS id FROM sma_products")->row();
        return $id;
    }

    public function max_id()
    {
        $id = $this->db->query("SELECT MAX(id) AS id FROM sma_products")->row();
        return $id;
    }

    public function delete_cart_checked($customer_id)
    {
        $query = $this->db->query("select * from sma_cart where customer_id= '$customer_id'")->row();
        if ($query) {
            $this->db->delete('sma_cart', array('customer_id' => $customer_id));
            return true;
        } else {
            return false;
        }
    }


    public function order_history($paramater)
    {
        $customer_id = $paramater['customer_id'];
        $limit = $paramater['limit'];
        $offset = $paramater['offset'];
        $sql_limit = "";
        if ($limit > 0 && $offset != "") {

            $page = ($offset - 1) * $limit;

            $sql_limit = "LIMIT $page,$limit";
        }

        $order = $this->db->query("
                                    SELECT
                                    si.id,
                                    s.sale_status AS status,
                                    DATE_FORMAT(s.`date`, '%M-%d-%Y') AS order_date,
                                    s.id AS sid
                                    FROM
                                    sma_sales s
                                    INNER JOIN 
                                    sma_sale_items si
                                    ON s.id = si.sale_id
                                    WHERE s.customer_id = '$customer_id'
                                    GROUP BY s.id
                                    {$sql_limit}
        ")->result();
        $list = [];
        $path = base_url('assets/uploads/');
        foreach ($order as $item) {
            $data = $this->db->query("SELECT
                                            si.id,
                                            si.product_id,
                                            si.product_name AS name,
                                            si.product_code AS code,
                                            FORMAT (si.net_unit_price, 2) AS price,
                                            FORMAT (si.quantity,0) AS qty,
                                            REPLACE (FORMAT ((si.quantity * si.unit_price),2),',','') AS subtotal_usa,
                                            REPLACE (FORMAT (IF(si.rate_rel,((si.quantity * si.unit_price) * si.rate_rel),((si.quantity * si.unit_price) * (SELECT rate FROM sma_currencies WHERE `code` = 'REL'))),0),',','')
                                            AS subtotal_rei
                                            FROM
                                            sma_sales s
                                            INNER JOIN 
                                            sma_sale_items si
                                            ON s.id = si.sale_id
                                            WHERE s.customer_id = '$customer_id' AND si.sale_id = $item->sid
        ")->result();
            $products = [];
            $total_usa = 0;
            $total_rei = 0;
            foreach ($data as $od) {
                $subtotal_riel = $this->format->riel_format($od->subtotal_rei);
                array_push($products, [
                    'id' => $od->product_id,
                    'name' => $od->name,
                    'code' => $od->code,
                    'price' => $od->price,
                    'qty' => $od->qty,
                    'subtotal_usa' => $od->subtotal_usa,
                    'subtotal_rei' => $subtotal_riel,
                ]);
                $total_usa += $od->subtotal_usa;
                $total_rei += $od->subtotal_rei;
            }
            $total_riel = $this->format->riel_format($total_rei);
            $list[] = [
                'order_id' => $item->sid,
                'order_date' => $item->order_date,
                'status' => $item->status,
                'list_product' => $products,
                'total_usa' => (string)$total_usa,
                'total_rei' => (string)$total_riel,
            ];
        }
        return $list;
    }

}
