<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class CheckoutController extends MY_Controller

{
    public function __construct()
    {
        parent::__construct();
        header('Content-Type: application/json');
        $this->load->model('api/CheckoutModel', 'checkout');
        $this->load->model('api/HomeModel', 'home_api');
    }

    public function get_p()
    {
        $curr = $this->checkout->curr_rate();
        echo json_encode(['list' => $curr]);
    }

    public function checkout()
    {
        $total = 0;


//        $product_id = $this->input->post('product_id');
//        $qty = $this->input->post('qty');
//
//        $delivery = $this->input->post('delivery');
        $customer_id_get = $this->input->get('customer_id');
        //==========checkout=======\\
        $body = file_get_contents('php://input');

        $tmp = json_decode($body, true);    // using a temp variable for testing
        $result = array_reverse($tmp);
        $object = (object)$result;

        $product_id = $object->product_id;
        $customer_id_post = $object->customer_id;
        $qty = $object->qty;
        $delivery = $object->delivery;
        $provinces = $object->provinces;
        $districts = $object->districts;
        $communes = $object->communes;
        $villages = $object->villages;
        $address = $object->address;
        $lat = $object->lat;
        $long = $object->long;
        $email = $object->email;
        $phone = $object->phone;

        if ($customer_id_get) {
            $customer_id = $customer_id_get;
        } else {
            $customer_id = $customer_id_post;
        }
        foreach ($product_id as $pid) {
            $minid = $this->checkout->min_id()->id;
            if ($pid < $minid) {
                echo json_encode(['unknown_this_product_id' => $pid]);
                return false;
            } else {

            }

        }

        $customer_details = $this->checkout->getCompanyByID($customer_id);
        $customer = !empty($customer_details->company) && $customer_details->company != '-' ? $customer_details->company : $customer_details->name;
        $data = array(
            'customer' => $customer,
            'total' => 0,
            'product_discount' => '',
            'order_discount_id' => '',
            'total_discount' => '',
            'order_discount' => '',
            'product_tax' => '',
            'order_tax_id' => '',
            'order_tax' => '',
            'total_tax' => '',
            'shipping' => $delivery,
            'grand_total' => 0,
        );
        $re = $this->checkout->save_checkout($customer_id, $data);

        //========== sale_items ==========\\

        for ($i = 0; $i < count($product_id); $i++) {
            $product_detail = $this->checkout->getProductNames($product_id[$i]);
            $curr = $this->checkout->curr_rate();
            $code = $product_detail->code;
            $name = $product_detail->name;
            $price_pro = $product_detail->price;
            $type = $product_detail->type;
            $rate = $curr->rate;

            $sub_total = $this->sma->formatDecimal(($price_pro * $qty[$i]), 4);

            $sale_items = array(
                'sale_id' => $re->id,
                'product_id' => $product_id[$i],
                'product_code' => $code,
                'product_name' => $name,
                'product_type' => $type,
                'option_id' => '',
                'rate_rel' => $rate,
                'net_unit_price' => $price_pro,
                'unit_price' => $price_pro,
                'quantity' => $qty[$i],
                'warehouse_id' => '',
                'item_tax' => '',
                'tax_rate_id' => '',
                'tax' => '',
                'discount' => '',
                'item_discount' => '',
                'subtotal' => $sub_total,
                'serial_no' => '',
                'real_unit_price' => '',
                'sale_item_id' => '',
                'product_unit_id' => '',
                'product_unit_code' => '',
                'unit_quantity' => '',
                'comment' => '',
                'used_stock' => '',
            );

            $total += $this->sma->formatDecimal(($sub_total), 2);
            $sale_itemss[] = $this->checkout->sale_items($sale_items);

        }

        $this->checkout->update_sale($total, $re);


        //=========save deliveries========\\
        $deliveries = array(
            'sale_id' => $re->id,
            'customer_id' => $customer_id,
            'provinces' => $provinces,
            'districts' => $districts,
            'communes' => $communes,
            'villages' => $villages,
            'address' => $address,
            'lat' => $lat,
            'long' => $long,
            'email' => $email,
            'phone' => $phone,

        );
        $data['content'] = $this->checkout->deliveries($deliveries);
        $this->checkout->delete_cart_checked($customer_id);
        $apply_online = $this->load->view($this->theme . 'api/delivery.php', $data, true);
        $rt = array($apply_online);
        echo json_encode(['sale' => $re, 'sale_items' => $sale_itemss, 'deliveries' => $rt], JSON_NUMERIC_CHECK);

    }

    public function list_order()
    {
        $ms = "list_order";
        $customer_id = $this->input->get('customer_id');
        $limit = $this->input->get('limit');
        $offset = $this->input->get('offset');
        $count_item = $this->home_api->count_row($customer_id);
        $total_price = $this->home_api->total_price($customer_id);
        $paramater = [
            'customer_id' => $customer_id,
            'limit' => $limit,
            'offset' => $offset,

        ];
        if ($customer_id) {
            $data = $this->checkout->order_history($paramater);
            if ($data) {
                echo json_encode(['count_item' => $count_item, 'total_price' => $total_price,$ms => $data]);
            } else {
                echo json_encode(['count_item' => $count_item, 'total_price' => $total_price,$ms => []]);
            }
        } else {
            echo json_encode(['count_item' => $count_item, 'total_price' => $total_price,$ms => []]);
        }

    }


}
