<?php defined('BASEPATH') OR exit('No direct script access allowed');

class CurrenciesFormat extends CI_Model
{

    public function __construct()
    {
        parent::__construct();

    }
    public function riel_format($format) {
        $ifelse = substr((string)$format,-2,2);
        if ($ifelse> 0) {
            $tr = $format + (100 - ($ifelse%100));
            return (string)$tr;
        }
        return $format;
    }
}